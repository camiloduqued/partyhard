<?php

class User {
    private $id;
    private $ip;
    
    function __construct($id, $ip) {
        $this->id = $id;
        $this->ip = $ip;
    }
    
    function getId() {
        return $this->id;
    }

    function getIp() {
        return $this->ip;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIp($ip) {
        $this->ip = $ip;
    }
    
    public static function get($id=null){
        $db = new MySQLiManager('localhost','root','','partyhard');
        if(is_null($id)){
            return $db->select("*", "user");
        }else{
            return $db->select("*", "user", "uuid = $id");
        }
    }
    
    public static function insert($uuid){
        $db = new MySQLiManager('localhost','root','','partyhard');
        if(!is_null($uuid)){
            $data = array('uuid'=>$uuid);
            return $db->insert("user", $data);
        } else{
            echo 'no puede ser null';
        }
    }
    
    public static function delete($uuid){
        $db = new MySQLiManager('localhost','root','','partyhard');
        if(!is_null($uuid)){
            $data = array('uuid'=>$uuid);
            return $db->delete("user", $data);
        } else{
            echo 'no puede ser null';
        }
    }

}
