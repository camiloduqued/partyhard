<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 *
 * @author pabhoz
 */
class SongsController 
{
    public function index()
    {
        echo "Songs Controller";
    }
    
    public function get($id = null){
        $r = Song::get($id);
        print json_encode($r);
    }
    public function getInPlaylist(){
        $r = Song::getInPlaylist();
        print json_encode($r);
    }
    
    public function getOutPlaylist(){
        $r = Song::getOutPlaylist();
        print json_encode($r);
    }
    
    public function updateInPlaylist(){
        if(isset($_POST)){
            $r = Song::updateInPlaylist($_POST['id'], $_POST['isInPlaylist']);
            echo $r.'esto si lo hizo';
        }  
    }
    
    public function getVotes($id = null){
        $r = Song::getVotes($id);
        print json_encode($r);
    }
    
    public function vote(){
        if(isset($_POST)){
            //echo 'POSTSITO';
            $r = Song::vote($_POST['id'], $_POST['vote']);
            echo $r.'esto si lo hizo';
        }  
    }
}
