<?php

class UserController 
{
    public function index()
    {
        echo "UserController";
    }
    
    public function get($id = null){
        $r = User::get($id);
        print json_encode($r);
    }
    
    public  function insert(){
        if(isset($_POST)){
            $r = User::insert($_POST['uuid']);
            print json_encode($r);
        }        
    }
    
    public  function delete(){
        if(isset($_POST)){
            $r = User::delete($_POST['uuid']);
            print json_encode($r);
        }        
    }
    
}