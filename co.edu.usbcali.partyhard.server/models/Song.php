<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Song
 *
 * @author camiloduqued
 */
class Song {
    private $title;
    private $artist;
    private $album;
    private $src;
    private $cover;
    private $pos;
    private $isInPlaylist;
    private $positiveVotes;
    private $negativeVotes;
    
    function __construct($title, $artist, $album, $src, $cover, $pos, $isInPlaylist, $positiveVotes, $negativeVotes) {
        $this->title = $title;
        $this->artist = $artist;
        $this->album = $album;
        $this->src = $src;
        $this->cover = $cover;
        $this->pos = $pos;
        $this->isInPlaylist = $isInPlaylist;
        $this->positiveVotes = $positiveVotes;
        $this->negativeVotes = $negativeVotes;
    }

        function getTitle() {
        return $this->title;
    }

    function getArtist() {
        return $this->artist;
    }

    function getAlbum() {
        return $this->album;
    }

    function getSrc() {
        return $this->src;
    }

    function getCover() {
        return $this->cover;
    }

    function getPos() {
        return $this->pos;
    }

    function getIsInPlaylist() {
        return $this->isInPlaylist;
    }

    function getPositiveVotes() {
        return $this->positiveVotes;
    }

    function getNegativeVotes() {
        return $this->negativeVotes;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setArtist($artist) {
        $this->artist = $artist;
    }

    function setAlbum($album) {
        $this->album = $album;
    }

    function setSrc($src) {
        $this->src = $src;
    }

    function setCover($cover) {
        $this->cover = $cover;
    }

    function setPos($pos) {
        $this->pos = $pos;
    }

    function setIsInPlaylist($isInPlaylist) {
        $this->isInPlaylist = $isInPlaylist;
    }

    function setPositiveVotes($positiveVotes) {
        $this->positiveVotes = $positiveVotes;
    }

    function setNegativeVotes($negativeVotes) {
        $this->negativeVotes = $negativeVotes;
    }

    
    
    
    public static function get($id=null){
        $db = new MySQLiManager('localhost','root','','partyhard');
        if(is_null($id)){
            return $db->select("*", "song");
        }else{
            return $db->select("*", "song", "id = $id");
        }
    }
    
    public static function getInPlaylist(){
        $db = new MySQLiManager('localhost','root','','partyhard');
        return $db->select("*", "song", "isInPlaylist = 1");
    }
    
    public static function getOutPlaylist(){
        $db = new MySQLiManager('localhost','root','','partyhard');
        return $db->select("*", "song", "isInPlaylist = 0");
    }
    
    public static function updateInPlaylist($id, $val){
        $db = new MySQLiManager('localhost','root','','partyhard');
        $data = array("isInPlaylist"=>$val);
        return $db->update("song", $data, "id = $id");
    }
        
    public static function getVotes($id){
        $db = new MySQLiManager('localhost','root','','partyhard');
        if(is_null($id)){
            return 'Necesita un id';
        } else {
            return $db->select("positiveVotes, negativeVotes", "song", "id = $id")[0];
            
        }
    }
    
    public static function vote($id, $vote){
        
        $votes = Song::getVotes($id);
        $db = new MySQLiManager('localhost','root','','partyhard');
        if($vote == 0){
            //Negativo
            $data = array("negativeVotes"=>($votes['negativeVotes'] + 1));
        } else if($vote == 1){
            //Positivo
            $data = array("positiveVotes"=>($votes['positiveVotes'] + 1));
        }
        
        return $db->update("song", $data, "id = $id");
    }
    
}
